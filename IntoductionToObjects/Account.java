public class Account{
   
        //Private Variables
        private double balance;
        private String name;

        //Getters and Setters

        public double getBalance() {
            return balance;
        }

        public void setBalance(double balance) {
            this.balance = balance;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        // Add Interest Method 

        public void addInterest(){

            balance = balance *1.10;

        
        }
        
    
}