public class TestAccount {
    public static void main(String[] args) {
        
        // New Account Object
        Account myAccount = new Account();
        

        //Get Methods
        myAccount.setBalance(500);
        myAccount.setName("Alice Archer");

        //Print 
        System.out.println("This Account Belongs to " + myAccount.getName());
        System.out.println("The Current Balance in the Account is " + myAccount.getBalance());

        // Adding the Interest
        myAccount.addInterest();
        System.out.println("With Added Interest the Balance is Now " + myAccount.getBalance());


    }
}