public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = {

            new Account("Jane", 500),
            new SavingsAccount("Charlotte", 200),
            new CurrentAccount("Madeline", 300)

        };

        for (int i=0; i<accounts.length; i++)
        {

            accounts[i].addInterest();
            System.out.println("This account belongs to " + accounts[i].getName());

        }
    }
}