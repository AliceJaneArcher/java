public class TestAccount2 {

    public static void main(final String[] args) {

        // New Account Array
        Account[] arrayOfAccounts;
        arrayOfAccounts = new Account[5];

        final double[] amounts = { 23, 5444, 2, 345, 34 };
        final String[] names = { "Picard", "Ryker", "Worf", "Troy", "Data" };

        for (int i = 0; i < arrayOfAccounts.length; i++) {

            arrayOfAccounts[i] = new Account(names[i], amounts[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].setName(names[i]);

            // Print
            System.out.println("This Account Belongs to " + arrayOfAccounts[i].getName());
            System.out.println("The Current Balance in the Account is " + arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();
            System.out.println("With Added Interest the Balance is Now " + arrayOfAccounts[i].getBalance());

        }

       

    }
 
    
}