public class Account{
    //Private Variables
    private double balance;
    private String name;

    private static double interestRate = 0.2;

    public Account(String s, double d){
        name = s;
        balance = d;
    }

    public Account(){
        name = "Alice";
        balance = 50;
    
    }

    //Getters and Setters

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    

    // Add Interest Method 

    public void addInterest(){

        balance = balance + (balance*interestRate);

    
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    
    
}