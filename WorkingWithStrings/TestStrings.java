public class TestStrings {
    public static void main(String[] args) {
        
        //change string form doc to bak
        StringBuilder docName = new StringBuilder("example.doc");
        StringBuilder strNew = docName.replace(8, 11, "bak");

        System.out.println("New Line Replacement = " + strNew.toString());

        //Comparing Strings
        String name1 = "Alice";
        String name2 = "Archer";

        boolean equalNames = (name1.equals(name2));
        System.out.println(equalNames);

        //Reocurring characters
        String sentence = "the quick brown fox swallowed down the lazy chicken";

        int stringCount = 0;
        for(int i =0; i<sentence.length(); i++){

            if(sentence.substring(i) == "ow"){

                stringCount++;
            }
        }

        System.out.println("Ow appears in the sentence " + stringCount + " times!");
    }
}