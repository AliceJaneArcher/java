public class ArrayOfAccounts
{
    public static void main(final String[] args) {

        // New Account Array
        Account[] arrayOfAccounts;
        arrayOfAccounts = new Account[5];

        final double[] amounts = { 23, 5444, 2, 345, 34 };
        final String[] names = { "Picard", "Ryker", "Worf", "Troy", "Data" };

        for (int i = 0; i < arrayOfAccounts.length; i++) {

            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].setName(names[i]);

            // Print
            System.out.println("This Account Belongs to " + arrayOfAccounts[i].getName());
            System.out.println("The Current Balance in the Account is " + arrayOfAccounts[i].getBalance());

            arrayOfAccounts[i].addInterest();
            System.out.println("With Added Interest the Balance is Now " + arrayOfAccounts[i].getBalance());

        }

    }
 
    

}

class Account{
   
    //Private Variables
    private double balance;
    private String name;

    //Getters and Setters

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Add Interest Method 

    public void addInterest(){

        balance = balance *1.10;

    
    }
    

}


